// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
//  це заміна в тексті спеціальних символів на відповідні текстові підстановки тоді,
// 2. Які засоби оголошення функцій ви знаєте?
//    function declaration, function expression, стрелочні функції.
// 3. Що таке hoisting, як він працює для змінних та функцій?
// це можливість отримувати доступ до функцій та змінних до того,
// як вони були створені. Це механізм відноситься лише до оголошення функцій та змінних.



function createNewUser() {
    let newUser = {
        firstName: prompt(`Введіть ваше ім'я?`),
        secondName: prompt(`Введіть ваше призвище`),
        birthday: prompt(`Введіть дату народження у форматі: dd.mm.yyyy`),
        getLogin: function () {
            return `${this.firstName[0]}${this.secondName}`.toLowerCase();
        },
        getAge: function () {
            return `${new Date().getFullYear() - newUser.birthday.slice(6)}`
        },
        getPassword: function () {
            return `${this.firstName[0].toUpperCase()}${this.secondName.toLowerCase()}${this.birthday.slice(6)}`
        }
    }
    return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
/*
   - Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
   - Які засоби оголошення функцій ви знаєте?
   - Що таке hoisting, як він працює для змінних та функцій?
 */